# Url Shortener

Application that allows shorting large urls to a short representation URL through a REST API in order to redirect with it to the original URL.

### Key Features

- REST API.
- Shorten a given URL.
- Redirect to the original url given the short URL.
- Delete existing short URL.

### Stack

- Java 11 (Main Language)
- Spring Boot (Rest API Framework)
- Redis (Cache Database)
- Mongo DB (Main Database)
- Netflix Eureka (Discovery Server)
- Spring Cloud Gateway (API Gateway)
- Docker (Containers)
- Docker Compose (Container Orchestration)
- Jacoco (Test Coverage).

## Requirements

Needs to install the following tools:

- Gradle 6.3
- Java 11 
- Docker
- Docker Compose
- Shell
- Curl

### Running the Solution

1. Clone the repository in your local operative system. It contains the projects:

    - url-shortener-api-gateway
    - url-shortener-devops
    - url-shortener-discovery-server
    - url-shortener-service

2. Open a terminal on the root folder of the repository.

3. Run the command "sh run.sh" that build the java projects using a gradle wrapper and orchestrate the initialization of the application processes using docker-compose.

4. Open in a browser the URL http://localhost:8085/ in order to see the Eureka Services Monitor. Check the applications "URL-SHORTENER-API-GATEWAY" and "URL-SHORTENER-SERVICE" are running.

5. Enjoy the application.

### Running the Solution (Express way)

If you desired to avoid the compilation of the java projects, you can use the java projects docker images contained in the solution following the next steps:

1. Clone the repository in your local operative system. It contains the projects:

    - url-shortener-api-gateway
    - url-shortener-devops
    - url-shortener-discovery-server
    - url-shortener-service

2. Open a terminal on the root folder of the repository.

3. Run the command "sh expressRun.sh" that orchestrate the initialization of the application processes using docker-compose.

4. Open in a browser the URL http://localhost:8085/ in order to see the Eureka Services Monitor. Check the applications "URL-SHORTENER-API-GATEWAY" and "URL-SHORTENER-SERVICE" are running.

5. Enjoy the application.


### Test the Solution

- Shorten URL Case: Run the next command in a terminal to get a shorter url from the REST API of a given URL replacing the <PUT-URL-HERE> element with the URL.

      curl -XPOST -H "Content-type: application/json" -d '{ "url": "<PUT-URL-HERE>" }' 'http://127.0.0.1:8086/api/v1/shorturls/'

- Get original URL through the REST API using the short URL delivered by the case Shorten URL Case: Run the next command to find the original URL replacing the <PUT-SHORT-URL-HASH-CODE-HERE> with the hashcode obtained in Shorten URL Case.

      curl -XGET -H "Content-type: application/json" 'http://127.0.0.1:8086/api/v1/shorturls/<PUT-SHORT-URL-HASH-CODE-HERE>' 

- Redirect short URL to original URL Case: Open a browser and enter the short url you received on Shorten URL Case. It will be redirected to the original URL.

- Delete shorter URL Case: Run the next command to delete the original URL replacing the <PUT-SHORT-URL-HASH-CODE-HERE> with the hashcode obtained in Shorten URL Case.

      curl -XDELETE -H "Content-type: application/json" 'http://127.0.0.1:8086/api/v1/shorturls/<PUT-SHORT-URL-HASH-CODE-HERE>'

### Solution Characteristics

- The solution is provided in 4 projects.

    * url-shortener-discovery-server: It allows keeping informed the services "url-shortener-api-gateway" and "url-shortener-service" that are available for use. This project is using the Netflix Eureka libraries to allows the initialization of a Discovery Server.
    
    * url-shortener-api-gateway: It allows to pass the request of the users with custom added headers to the different services that offers the REST API, depends on the requirements of the user and services. This project is using the Spring Cloud Gateway to allows the initialization of a API Gateway.
    
    * url-shortener-devops: Contains neccesary docker-compose scripts and docker images to orchestrate the initialization of the services without downloading general docker images.
    
    * url-shortener-service: Represent the REST API for the shorten URL processes.
    
- Details of "url-shortener-service" project:

    * The project is divided in packages following a CLEAN CODE architecture as follows:
    
        - configuration: Package contains the configuration of the project.
            * MongoDBConfiguration: Contains the necessary configuration to use a Mongo Database.
            * RedisCacheConfiguration: Contains the necessary configuration to use Redis Cache Database including the port and the hostname.
            * PluginsDependencyInjectionConfiguration: Contains the dependency injection of the implemented plugins for the services needed in the domain layer.
            * RepositoriesDependencyInjectionConfiguration: Contains the dependency injection of the repositories.
            * UseCasesDependencyInjectionConfiguration: Contains the dependency injection of the implemented use cases that offer the domain layer.

        - core: Package contains the general common logic to be used in different layers.
        
        - core.plugins: Package contains the implementation knows as Plugins of the services that are needed by the Domain layer.
        
        - data: Package contains the logic related to the data layer.
        
        - data.storage.dao.HashedUrlMongoRedisDAO: Manage the persistence on the Mongo Database and Redis Cache.
        
        - data.storage.models: Package that contains the model representation of the persisted data in the databases.
        
        - data.storage.repositories: Package contains the implementations of the repositories needed by the Domain Layer.
        
        - domain: Contains the business logic of the entire microservice. It doesn't contain any topic related to frameworks, services or databases connections.
        
        - domain.entities: Contains the data structures related to the business.
        
        - domain.exceptions: Contains the exceptions the business logic throws.
        
        - domain.gateways: Interfaces that allow to communicate the business to the external services without knowing its implementation.
        
        - domain.usecases: Classes that represents every process the business can do.
        
        - entrypoint.api: Contains the presentation logic. In this case related to the REST API.
        
        - entrypoint.api.controller: Contains the logic that manages the request and responses of the REST API.
        
        - entrypoint.api.dto: Contains the complex data structures that can be received or response the REST API.
        
        - entrypoint.api.handlers.RestExceptionHandler: Class manages how the business logic exceptions are represented in a REST API response.
        
        - properties: Contains some business logic properties mapped related to the project

### Running the Unit Tests

To run the Unit Tests of the project "url-shortener-service", follow the next steps:

1. Import the project "url-shortener-service" as gradle project in Intelli J Community Edition.

2. Right click on the project and select "Run Tests".

3. It will show the results of the unit tests.

4. Can see the test report opening the file: <solution-root>/url-shortener-service/build/reports/tests/test/index.html in a browser.

5. Can see the jacoco test report coverage opening the file: <solution-root>/url-shortener-service/build/reports/jacoco/test/html/index.html in a browser.

### Running Postman Tests:

Additional to Unit Tests, the solution can be tested using a Postman configuration attached to the solution called "URLShortener.postman_collection.json".

To run the Postman Tests:

1. Import the file "URLShortener.postman_collection.json" in Postman application.

2. Click on the Play icon inside the collection. It will run the cases "ShortenUrl", "GetUrlByShortLinkHashCode" and "DeleteUrlByShortLinkHashCode" in the give order.

3. Verified the tests are correct.

### Improvement to be done:

- Allow a service whose gives statistic information about the shorter urls.
- Allows HTTPS.
- Implement OAuth2 with the client flow creating a project who manage an OAuth2 Server and updating the microservices to be added as Resource Servers in order to add security between the microservices.  
- Add Logging for the different projects.
- Add a Logging stack using FluentD, ElasticSearch and Grafana to monitor the logs of the solution.

### Authors

* **Alejandro Linares** - *Senior Software Developer*

* **alejandrolinaresvillamizar@gmail.com** - *Skype: alejandrolinaresvillamizar*

