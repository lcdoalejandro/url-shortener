echo "Building url-shortener-api-gateway project"
cd url-shortener-api-gateway && ./gradlew clean build && cd ..

echo "\n Building url-shortener-discovery-server project"
cd url-shortener-discovery-server && ./gradlew clean build && cd ..

echo "\n Building url-shortener-service project"
cd url-shortener-service && ./gradlew clean build && cd ..

echo "\n Loading openjdk11 docker image"
docker load -i url-shortener-devops/docker-images/openjdk11.docker

echo "\n Loading mongo3.6 docker image"
docker load -i url-shortener-devops/docker-images/mongo3.6.docker

echo "\n Loading redis docker image"
docker load -i url-shortener-devops/docker-images/redis.docker

echo "\n Starting services orchestration"
docker-compose -f url-shortener-devops/docker-compose.yml up
