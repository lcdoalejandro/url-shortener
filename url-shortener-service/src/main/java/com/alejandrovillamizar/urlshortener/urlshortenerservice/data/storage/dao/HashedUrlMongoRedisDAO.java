package com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.dao;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.interfaces.MongoHashedUrlRepository;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.models.HashedUrlModel;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.Date;
import java.util.Optional;

public class HashedUrlMongoRedisDAO {

    private final MongoHashedUrlRepository mongoHashedUrlRepository;

    public HashedUrlMongoRedisDAO(MongoHashedUrlRepository mongoHashedUrlRepository) {
        this.mongoHashedUrlRepository = mongoHashedUrlRepository;
    }

    public HashedUrlModel create(HashedUrlModel hashedUrlModel) {

        hashedUrlModel.setCreateAt(new Date());

        return this.mongoHashedUrlRepository.save(hashedUrlModel);
    }

    public Boolean existsHashedUrlHashCode(String hashCode) {

        Optional<HashedUrlModel> maybeHashedUrlModel = this.mongoHashedUrlRepository.findById(hashCode);

        return maybeHashedUrlModel.isPresent();
    }

    @Cacheable(value = "hashCode", key = "#hashCode", unless = "#result == null")
    public Optional<HashedUrlModel> findHashedUrlByHashCode(String hashCode) {

        Optional<HashedUrlModel> maybeHashedUrlModel = this.mongoHashedUrlRepository.findById(hashCode);

        return maybeHashedUrlModel;
    }

    @CacheEvict(value = "hashCode", key = "#hashedUrlModel.hashCode")
    public void delete(HashedUrlModel hashedUrlModel) {
        this.mongoHashedUrlRepository.delete(hashedUrlModel);
    }
}
