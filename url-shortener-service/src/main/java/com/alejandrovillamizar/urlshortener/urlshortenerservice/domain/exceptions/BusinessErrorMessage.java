package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions;

public enum BusinessErrorMessage {

    REGISTRY_NOT_FOUND("Registry requested not found"),
    HASH_CODE_CREATION_MAXIMUM_RETRIES_REACHED("Hash code creation maximum retries reached");

    private String errorMessage;

    BusinessErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
