package com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.repositories;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.dao.HashedUrlMongoRedisDAO;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.models.HashedUrlModel;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.HashedUrlEntity;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.UrlEntity;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.HashedUrlRepository;

import java.util.Optional;

public class HashedUrlMongoRedisRepositoryImpl implements HashedUrlRepository {

    private final HashedUrlMongoRedisDAO hashedUrlMongoRedisDAO;

    public HashedUrlMongoRedisRepositoryImpl(HashedUrlMongoRedisDAO hashedUrlMongoRedisDAO) {
        this.hashedUrlMongoRedisDAO = hashedUrlMongoRedisDAO;
    }

    @Override
    public HashedUrlEntity create(HashedUrlEntity hashedUrlEntity) {

        HashedUrlModel hashedUrlModel = HashedUrlModel.of(hashedUrlEntity);

        return hashedUrlMongoRedisDAO.create(hashedUrlModel).toHashedUrlEntity();
    }

    @Override
    public Boolean existsHashCode(String hashCode) {
        return this.hashedUrlMongoRedisDAO.existsHashedUrlHashCode(hashCode);
    }

    @Override
    public Optional<UrlEntity> getUrlByHashCode(String hashCode) {

        Optional<HashedUrlModel> hashedUrlModel = this.hashedUrlMongoRedisDAO.findHashedUrlByHashCode(hashCode);

        return hashedUrlModel.map(model -> model.toUrlEntity());
    }

    @Override
    public Optional<HashedUrlEntity> getHashedUrlByHashCode(String hashCode) {

        Optional<HashedUrlModel> hashedUrlModel = this.hashedUrlMongoRedisDAO.findHashedUrlByHashCode(hashCode);

        return hashedUrlModel.map(model -> model.toHashedUrlEntity());
    }

    @Override
    public void delete(HashedUrlEntity hashedUrlEntity) {
        this.hashedUrlMongoRedisDAO.delete(HashedUrlModel.of(hashedUrlEntity));
    }
}