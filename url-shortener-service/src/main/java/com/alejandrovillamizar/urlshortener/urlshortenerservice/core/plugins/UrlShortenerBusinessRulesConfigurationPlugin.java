package com.alejandrovillamizar.urlshortener.urlshortenerservice.core.plugins;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.BusinessRulesConfigurationService;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.properties.UrlShortenerApplicationProperties;

public class UrlShortenerBusinessRulesConfigurationPlugin implements BusinessRulesConfigurationService {

    private final UrlShortenerApplicationProperties urlShortenerApplicationProperties;

    public UrlShortenerBusinessRulesConfigurationPlugin(
            UrlShortenerApplicationProperties urlShortenerApplicationProperties) {

        this.urlShortenerApplicationProperties = urlShortenerApplicationProperties;
    }

    @Override
    public String getDomain() {
        return urlShortenerApplicationProperties.getHostUrl();
    }

    @Override
    public Integer getHashCodeCreationMaximumRetries() {
        return urlShortenerApplicationProperties.getHashCodeCreationMaximumRetries();
    }
}
