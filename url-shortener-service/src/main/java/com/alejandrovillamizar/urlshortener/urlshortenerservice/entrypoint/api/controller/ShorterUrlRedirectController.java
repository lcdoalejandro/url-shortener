package com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.controller;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.UrlEntity;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.BusinessException;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.RegistryNotFoundException;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases.GetUrlByShortLinkHashCodeUseCase;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class ShorterUrlRedirectController {

    private final GetUrlByShortLinkHashCodeUseCase getUrlByShortLinkHashCodeUseCase;

    public ShorterUrlRedirectController(
            GetUrlByShortLinkHashCodeUseCase getUrlByShortLinkHashCodeUseCase) {

        this.getUrlByShortLinkHashCodeUseCase = getUrlByShortLinkHashCodeUseCase;
    }

    @GetMapping("{shortLinkHashCode}")
    public ResponseEntity<Void> redirectShorterUrl(@PathVariable("shortLinkHashCode") final String shortLinkHashCode) {

        ResponseEntity response;

        UrlEntity urlEntity;
        try {
            urlEntity = this.getUrlByShortLinkHashCodeUseCase.execute(
                    new GetUrlByShortLinkHashCodeUseCase.InputValues(shortLinkHashCode)
            ).getUrlEntity();

            response = ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY)
                    .header(HttpHeaders.LOCATION, urlEntity.getUrl()).build();

        } catch (RegistryNotFoundException registryNotFoundException) {
            response = ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.TEXT_PLAIN)
                    .body("Content doesn't exists, try another URL");
        } catch (BusinessException businessException) {
            response = ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        return response;
    }
}
