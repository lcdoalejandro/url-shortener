package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.HashedUrlEntity;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.UrlEntity;

import java.util.Optional;

public interface HashedUrlRepository {

    HashedUrlEntity create(HashedUrlEntity hashedUrlEntity);

    Boolean existsHashCode(String hashCode);

    Optional<UrlEntity> getUrlByHashCode(String hashCode);

    Optional<HashedUrlEntity> getHashedUrlByHashCode(String hashCode);

    void delete(HashedUrlEntity hashedUrlEntity);
}
