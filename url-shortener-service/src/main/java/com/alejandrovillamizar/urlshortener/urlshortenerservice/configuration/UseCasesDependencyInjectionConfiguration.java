package com.alejandrovillamizar.urlshortener.urlshortenerservice.configuration;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.BusinessRulesConfigurationService;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.HashedUrlHashCodeGeneratorService;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.HashedUrlRepository;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases.DeleteUrlByShortLinkHashCodeUseCase;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases.GetUrlByShortLinkHashCodeUseCase;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases.ShortenUrlUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCasesDependencyInjectionConfiguration {

    @Bean
    public ShortenUrlUseCase shortenUrlUseCase(HashedUrlRepository hashedUrlRepository,
                                               HashedUrlHashCodeGeneratorService hashedUrlHashCodeGeneratorService,
                                               BusinessRulesConfigurationService businessRulesConfigurationService) {

        return new ShortenUrlUseCase(
                hashedUrlRepository, hashedUrlHashCodeGeneratorService, businessRulesConfigurationService);
    }

    @Bean
    public GetUrlByShortLinkHashCodeUseCase getUrlByShortLinkHashCodeUseCase(HashedUrlRepository hashedUrlRepository) {

        return new GetUrlByShortLinkHashCodeUseCase(hashedUrlRepository);
    }

    @Bean
    DeleteUrlByShortLinkHashCodeUseCase deleteUrlByShortLinkHashCodeUseCase(HashedUrlRepository hashedUrlRepository,
                                                                            BusinessRulesConfigurationService businessRulesConfigurationService) {

        return new DeleteUrlByShortLinkHashCodeUseCase(hashedUrlRepository, businessRulesConfigurationService);
    }
}
