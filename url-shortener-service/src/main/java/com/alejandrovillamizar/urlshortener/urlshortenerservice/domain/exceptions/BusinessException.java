package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions;

public class BusinessException extends Exception {

    private final BusinessErrorMessage errorMessage;

    private final String detail;

    public BusinessException(BusinessErrorMessage errorMessage, String detail) {
        super(errorMessage.getErrorMessage());
        this.errorMessage = errorMessage;
        this.detail = detail;
    }

    public BusinessErrorMessage getErrorMessageEnumerator() {
        return this.errorMessage;
    }

    public String getErrorName() {
        return this.errorMessage.name();
    }

    public String getErrorMessage() {
        return this.errorMessage.getErrorMessage();
    }

    public String getDetail() {
        return this.detail;
    }
}
