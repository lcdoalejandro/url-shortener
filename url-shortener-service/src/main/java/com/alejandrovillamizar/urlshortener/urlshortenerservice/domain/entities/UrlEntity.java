package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UrlEntity {

    private final String url;
}
