package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions;

public class RegistryNotFoundException extends BusinessException {

    public RegistryNotFoundException() {
        super(BusinessErrorMessage.REGISTRY_NOT_FOUND, "");
    }

    public RegistryNotFoundException(String detail) {
        super(BusinessErrorMessage.REGISTRY_NOT_FOUND, detail);
    }
}
