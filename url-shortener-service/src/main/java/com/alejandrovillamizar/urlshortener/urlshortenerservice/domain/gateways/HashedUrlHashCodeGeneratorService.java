package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways;

public interface HashedUrlHashCodeGeneratorService {

    String generateHashCode();
}
