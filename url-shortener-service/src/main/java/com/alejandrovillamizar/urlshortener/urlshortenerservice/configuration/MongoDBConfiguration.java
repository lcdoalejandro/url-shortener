package com.alejandrovillamizar.urlshortener.urlshortenerservice.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.interfaces")
public class MongoDBConfiguration {

}
