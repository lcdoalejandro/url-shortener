package com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.models;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.HashedUrlEntity;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.UrlEntity;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Document(collection = "hashedurls")
@JsonPropertyOrder({"hashCode", "url", "createAt"})
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class HashedUrlModel implements Serializable {

    @Id
    private String hashCode;

    @NotNull
    private String url;

    private Date createAt;

    public static HashedUrlModel of(HashedUrlEntity hashedUrlEntity) {

        return HashedUrlModel.builder()
                .hashCode(hashedUrlEntity.getHashCode())
                .url(hashedUrlEntity.getUrl())
                .build();
    }

    public HashedUrlEntity toHashedUrlEntity() {

        return HashedUrlEntity.builder()
                .hashCode(this.getHashCode())
                .url(this.getUrl())
                .build();
    }

    public UrlEntity toUrlEntity() {

        return UrlEntity.builder()
                .url(this.getUrl())
                .build();
    }
}
