package com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.dto;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.UrlEntity;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Builder
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)

public class UrlDto {

    @NotBlank(message = "Url is mandatory")
    private String url;

    public static UrlDto of(final UrlEntity urlEntity) {

        return UrlDto.builder()
                .url(urlEntity.getUrl())
                .build();
    }
}
