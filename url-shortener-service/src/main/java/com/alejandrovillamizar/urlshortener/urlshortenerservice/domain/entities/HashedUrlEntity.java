package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class HashedUrlEntity {

    private final String hashCode;

    private final String url;
}
