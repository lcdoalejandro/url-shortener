package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ShortLinkEntity {

    private final String hashCode;

    private final String url;

    private final String shortUrl;
}
