package com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.handlers;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.core.model.ApiError;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.BusinessException;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.HashCodeCreationMaximumRetriesReachedException;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.RegistryNotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = "Malformed JSON request";
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
    }

    @ExceptionHandler({
            HashCodeCreationMaximumRetriesReachedException.class
    })
    protected ResponseEntity<Object> handleHashCodeCreationMaximumRetriesReachedException(BusinessException ex) {

        ApiError apiError = new ApiError(HttpStatus.CONFLICT, ex.getErrorName(), ex.getErrorMessage(),
                ex.getDetail());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler({
            RegistryNotFoundException.class
    })
    protected ResponseEntity<Object> handleNotFoundException(BusinessException ex) {

        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getErrorName(), ex.getErrorMessage(),
                ex.getDetail());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception e) {

        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR);
        return buildResponseEntity(apiError);
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }
}
