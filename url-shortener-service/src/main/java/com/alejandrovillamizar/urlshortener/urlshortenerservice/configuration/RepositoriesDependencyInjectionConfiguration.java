package com.alejandrovillamizar.urlshortener.urlshortenerservice.configuration;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.dao.HashedUrlMongoRedisDAO;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.interfaces.MongoHashedUrlRepository;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.repositories.HashedUrlMongoRedisRepositoryImpl;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.HashedUrlRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RepositoriesDependencyInjectionConfiguration {

    @Bean
    HashedUrlMongoRedisDAO hashedUrlMongoRedisDAO(MongoHashedUrlRepository mongoHashedUrlRepository) {
        return new HashedUrlMongoRedisDAO(mongoHashedUrlRepository);
    }

    @Bean
    public HashedUrlRepository hashedUrlRepository(HashedUrlMongoRedisDAO hashedUrlMongoRedisDAO) {
        return new HashedUrlMongoRedisRepositoryImpl(hashedUrlMongoRedisDAO);
    }
}