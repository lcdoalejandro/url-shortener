package com.alejandrovillamizar.urlshortener.urlshortenerservice.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Configuration
@ConfigurationProperties(prefix = "urlshortener")
@Validated
@Getter
@Setter
public class UrlShortenerApplicationProperties {

    @NotBlank
    private String hostUrl;

    @Min(6)
    @Max(20)
    private Integer hashCodeCharactersLimit;

    @Min(1)
    @Max(1000)
    private Integer hashCodeCreationMaximumRetries;
}
