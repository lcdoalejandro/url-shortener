package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways;

public interface BusinessRulesConfigurationService {

    String getDomain();

    Integer getHashCodeCreationMaximumRetries();
}
