package com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.controller;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.ShortLinkEntity;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.UrlEntity;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.BusinessException;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases.DeleteUrlByShortLinkHashCodeUseCase;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases.GetUrlByShortLinkHashCodeUseCase;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases.ShortenUrlUseCase;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.dto.ShortLinkDto;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.dto.UrlDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/shorturls")
public class UrlShortenerController {

    private final ShortenUrlUseCase shortenUrlUseCase;
    private final GetUrlByShortLinkHashCodeUseCase getUrlByShortLinkHashCodeUseCase;
    private final DeleteUrlByShortLinkHashCodeUseCase deleteUrlByShortLinkHashCodeUseCase;

    public UrlShortenerController(
            ShortenUrlUseCase shortenUrlUseCase,
            GetUrlByShortLinkHashCodeUseCase getUrlByShortLinkHashCodeUseCase,
            DeleteUrlByShortLinkHashCodeUseCase deleteUrlByShortLinkHashCodeUseCase) {

        this.shortenUrlUseCase = shortenUrlUseCase;
        this.getUrlByShortLinkHashCodeUseCase = getUrlByShortLinkHashCodeUseCase;
        this.deleteUrlByShortLinkHashCodeUseCase = deleteUrlByShortLinkHashCodeUseCase;
    }

    @PostMapping("/")
    public ResponseEntity<ShortLinkDto> shortenUrl(
            @RequestBody @Validated final UrlDto urlDto) throws BusinessException {

        ShortLinkEntity shortLinkEntity = this.shortenUrlUseCase.execute(
                new ShortenUrlUseCase.InputValues(
                        urlDto.getUrl()
                )
        ).getShortLinkEntity();

        return ResponseEntity.status(HttpStatus.CREATED).body(ShortLinkDto.of(shortLinkEntity));
    }

    @GetMapping("/{shortLinkHashCode}")
    public ResponseEntity<UrlDto> getUrlByShortLinkHashCode(
            @PathVariable("shortLinkHashCode") final String shortLinkHashCode) throws BusinessException {

        UrlEntity urlEntity = this.getUrlByShortLinkHashCodeUseCase.execute(
                new GetUrlByShortLinkHashCodeUseCase.InputValues(shortLinkHashCode)
        ).getUrlEntity();

        return ResponseEntity.status(HttpStatus.FOUND).body(UrlDto.of(urlEntity));
    }

    @DeleteMapping("/{shortLinkHashCode}")
    public ResponseEntity<Void> deleteUrlByShortLinkHashCode(
            @PathVariable("shortLinkHashCode") final String shortLinkHashCode) throws BusinessException {

        this.deleteUrlByShortLinkHashCodeUseCase.execute(
                new DeleteUrlByShortLinkHashCodeUseCase.InputValues(shortLinkHashCode));

        return ResponseEntity.noContent().build();
    }
}
