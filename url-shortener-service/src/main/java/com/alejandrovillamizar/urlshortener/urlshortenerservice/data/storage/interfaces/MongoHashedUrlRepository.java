package com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.interfaces;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.models.HashedUrlModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoHashedUrlRepository extends MongoRepository<HashedUrlModel, String> {

}
