package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.core.usecases.UseCase;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.HashedUrlEntity;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.BusinessException;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.RegistryNotFoundException;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.BusinessRulesConfigurationService;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.HashedUrlRepository;
import lombok.Value;

import java.util.Optional;

public class DeleteUrlByShortLinkHashCodeUseCase
        extends UseCase<DeleteUrlByShortLinkHashCodeUseCase.InputValues, DeleteUrlByShortLinkHashCodeUseCase.OutputValues> {

    private final HashedUrlRepository hashedUrlRepository;
    private final BusinessRulesConfigurationService businessRulesConfigurationService;

    public DeleteUrlByShortLinkHashCodeUseCase(HashedUrlRepository hashedUrlRepository,
                                               BusinessRulesConfigurationService businessRulesConfigurationService) {
        this.hashedUrlRepository = hashedUrlRepository;
        this.businessRulesConfigurationService = businessRulesConfigurationService;
    }

    @Override
    public OutputValues execute(InputValues inputValues) throws BusinessException {

        String shortLinkHashCode = inputValues.getShortLinkHashCode();

        Optional<HashedUrlEntity> maybeHashedUrlEntity = this.hashedUrlRepository.
                getHashedUrlByHashCode(shortLinkHashCode);

        HashedUrlEntity hashedUrlEntity = maybeHashedUrlEntity.orElseThrow(RegistryNotFoundException::new);

        this.hashedUrlRepository.delete(hashedUrlEntity);

        return new DeleteUrlByShortLinkHashCodeUseCase.OutputValues();
    }

    @Value
    public static class InputValues implements UseCase.InputValues {
        private final String shortLinkHashCode;
    }

    public static class OutputValues implements UseCase.OutputValues {

    }
}