package com.alejandrovillamizar.urlshortener.urlshortenerservice.core.usecases;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.BusinessException;

public abstract class UseCase<Input extends UseCase.InputValues, Output extends UseCase.OutputValues> {

    public abstract Output execute(Input input) throws BusinessException;

    public interface InputValues {
    }

    public interface OutputValues {
    }
}
