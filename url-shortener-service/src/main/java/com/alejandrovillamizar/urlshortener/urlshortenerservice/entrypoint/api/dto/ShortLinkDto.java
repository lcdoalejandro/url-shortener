package com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.dto;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.ShortLinkEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ShortLinkDto {

    private String hashCode;
    private String url;
    private String shortUrl;

    public static ShortLinkDto of(final ShortLinkEntity shortLinkEntity) {
        return ShortLinkDto.builder()
                .hashCode(shortLinkEntity.getHashCode())
                .url(shortLinkEntity.getUrl())
                .shortUrl(shortLinkEntity.getShortUrl())
                .build();
    }

}
