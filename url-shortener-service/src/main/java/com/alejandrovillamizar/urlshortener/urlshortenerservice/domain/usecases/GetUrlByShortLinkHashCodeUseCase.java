package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.core.usecases.UseCase;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.UrlEntity;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.BusinessException;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.RegistryNotFoundException;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.HashedUrlRepository;
import lombok.Value;

import java.util.Optional;

public class GetUrlByShortLinkHashCodeUseCase extends UseCase<GetUrlByShortLinkHashCodeUseCase.InputValues,
        GetUrlByShortLinkHashCodeUseCase.OutputValues> {

    private final HashedUrlRepository hashedUrlRepository;

    public GetUrlByShortLinkHashCodeUseCase(HashedUrlRepository hashedUrlRepository) {
        this.hashedUrlRepository = hashedUrlRepository;
    }

    @Override
    public OutputValues execute(InputValues inputValues) throws BusinessException {

        String shortLinkHashCode = inputValues.getShortLinkHashCode();

        Optional<UrlEntity> maybeUrlEntity = this.hashedUrlRepository.getUrlByHashCode(shortLinkHashCode);

        return new OutputValues(maybeUrlEntity.orElseThrow(RegistryNotFoundException::new));
    }

    @Value
    public static class InputValues implements UseCase.InputValues {
        private final String shortLinkHashCode;
    }

    @Value
    public static class OutputValues implements UseCase.OutputValues {
        private final UrlEntity urlEntity;
    }

}
