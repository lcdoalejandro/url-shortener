package com.alejandrovillamizar.urlshortener.urlshortenerservice.configuration;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.core.plugins.RandomHashedUrlHashCodeGeneratorPlugin;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.core.plugins.UrlShortenerBusinessRulesConfigurationPlugin;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.BusinessRulesConfigurationService;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.HashedUrlHashCodeGeneratorService;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.properties.UrlShortenerApplicationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PluginsDependencyInjectionConfiguration {

    @Bean
    HashedUrlHashCodeGeneratorService hashedUrlHashCodeGeneratorService(
            UrlShortenerApplicationProperties urlShortenerApplicationProperties) {
        return new RandomHashedUrlHashCodeGeneratorPlugin(urlShortenerApplicationProperties);
    }

    @Bean
    BusinessRulesConfigurationService businessRulesConfigurationService(
            UrlShortenerApplicationProperties urlShortenerApplicationProperties) {
        return new UrlShortenerBusinessRulesConfigurationPlugin(urlShortenerApplicationProperties);
    }
}