package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.core.usecases.UseCase;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.HashedUrlEntity;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.entities.ShortLinkEntity;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.BusinessException;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.HashCodeCreationMaximumRetriesReachedException;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.BusinessRulesConfigurationService;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.HashedUrlHashCodeGeneratorService;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.HashedUrlRepository;
import lombok.Value;

public class ShortenUrlUseCase extends UseCase<ShortenUrlUseCase.InputValues, ShortenUrlUseCase.OutputValues> {

    private final HashedUrlRepository hashedUrlRepository;
    private final HashedUrlHashCodeGeneratorService hashedUrlHashCodeGeneratorService;
    private final BusinessRulesConfigurationService businessRulesConfigurationService;

    public ShortenUrlUseCase(HashedUrlRepository hashedUrlRepository,
                             HashedUrlHashCodeGeneratorService hashedUrlHashCodeGeneratorService,
                             BusinessRulesConfigurationService businessRulesConfigurationService) {
        this.hashedUrlRepository = hashedUrlRepository;
        this.hashedUrlHashCodeGeneratorService = hashedUrlHashCodeGeneratorService;
        this.businessRulesConfigurationService = businessRulesConfigurationService;
    }

    @Override
    public OutputValues execute(InputValues inputValues) throws BusinessException {

        String url = inputValues.getUrl();

        HashedUrlEntity hashedUrlEntity = hashedUrlRepository.create(HashedUrlEntity.builder()
                .hashCode(this.generateFreeHashCode())
                .url(url)
                .build());

        String shortUrl = new StringBuilder()
                .append(businessRulesConfigurationService.getDomain())
                .append(hashedUrlEntity.getHashCode())
                .toString();

        ShortLinkEntity shortLinkEntity = ShortLinkEntity.builder()
                .hashCode(hashedUrlEntity.getHashCode())
                .url(hashedUrlEntity.getUrl())
                .shortUrl(shortUrl)
                .build();

        return new OutputValues(shortLinkEntity);
    }

    private String generateFreeHashCode() throws HashCodeCreationMaximumRetriesReachedException {

        String freeHashCode;
        Integer hashCodeCreationRetries = 0;

        Integer hashCodeCreationMaximumRetries = this.businessRulesConfigurationService
                .getHashCodeCreationMaximumRetries();

        do {
            freeHashCode = this.hashedUrlHashCodeGeneratorService.generateHashCode();

            if (!this.hashedUrlRepository.existsHashCode(freeHashCode)) {
                return freeHashCode;
            }

            hashCodeCreationRetries++;

        } while (hashCodeCreationRetries <= hashCodeCreationMaximumRetries);

        throw new HashCodeCreationMaximumRetriesReachedException();
    }

    @Value
    public static class InputValues implements UseCase.InputValues {
        private final String url;
    }

    @Value
    public static class OutputValues implements UseCase.OutputValues {
        private final ShortLinkEntity shortLinkEntity;
    }

}