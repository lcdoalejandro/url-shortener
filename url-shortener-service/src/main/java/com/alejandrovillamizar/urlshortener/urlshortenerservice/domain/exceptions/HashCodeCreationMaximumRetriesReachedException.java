package com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions;

public class HashCodeCreationMaximumRetriesReachedException extends BusinessException {

    public HashCodeCreationMaximumRetriesReachedException() {
        super(BusinessErrorMessage.HASH_CODE_CREATION_MAXIMUM_RETRIES_REACHED, "");
    }

    public HashCodeCreationMaximumRetriesReachedException(String detail) {
        super(BusinessErrorMessage.HASH_CODE_CREATION_MAXIMUM_RETRIES_REACHED, detail);
    }
}
