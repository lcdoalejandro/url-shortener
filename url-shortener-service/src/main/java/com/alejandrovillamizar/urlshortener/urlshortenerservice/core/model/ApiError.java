package com.alejandrovillamizar.urlshortener.urlshortenerservice.core.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
public class ApiError {

    private HttpStatus httpStatus;

    private String status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime timestamp;

    private String message;
    private String detail;
    private String debugMessage;

    private ApiError() {
        timestamp = LocalDateTime.now();
    }

    public ApiError(HttpStatus httpStatus) {
        this();
        this.httpStatus = httpStatus;
    }

    public ApiError(HttpStatus httpStatus, String status, String message, String detail) {
        this();
        this.httpStatus = httpStatus;
        this.status = status;
        this.message = message;
        this.detail = detail;
    }

    public ApiError(HttpStatus httpStatus, String status, Throwable ex) {
        this();
        this.httpStatus = httpStatus;
        this.status = status;
        this.debugMessage = ex.getLocalizedMessage();
    }
}
