package com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.configuration.MongoDBConfiguration;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.dao.HashedUrlMongoRedisDAO;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.interfaces.MongoHashedUrlRepository;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.models.HashedUrlModel;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.BusinessRulesConfigurationService;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.HashedUrlHashCodeGeneratorService;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.gateways.HashedUrlRepository;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases.DeleteUrlByShortLinkHashCodeUseCase;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases.GetUrlByShortLinkHashCodeUseCase;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.usecases.ShortenUrlUseCase;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@EnableCaching
public abstract class AbstractBasicTest {

    @Autowired
    protected MockMvc mockMvc;

    @MockBean
    protected LettuceConnectionFactory redisConnectionFactory;

    @MockBean
    protected RedisTemplate<String, HashedUrlModel> hashedUrlModelRedisTemplate;

    @MockBean
    protected org.springframework.data.redis.cache.RedisCacheConfiguration cacheConfiguration;

    @MockBean
    protected CacheManager cacheManager;

    @MockBean
    protected MongoHashedUrlRepository mongoHashedUrlRepository;

    @MockBean
    protected MongoDBConfiguration mongoDBConfiguration;

    @MockBean
    protected MongoClient mongoClient;

    @Autowired
    protected HashedUrlHashCodeGeneratorService hashedUrlHashCodeGeneratorService;

    @Autowired
    protected BusinessRulesConfigurationService businessRulesConfigurationService;

    @Autowired
    protected HashedUrlMongoRedisDAO hashedUrlMongoRedisDAO;

    @Autowired
    protected HashedUrlRepository hashedUrlRepository;

    @Autowired
    protected ShortenUrlUseCase shortenUrlUseCase;

    @Autowired
    protected GetUrlByShortLinkHashCodeUseCase getUrlByShortLinkHashCodeUseCase;

    @Autowired
    protected DeleteUrlByShortLinkHashCodeUseCase deleteUrlByShortLinkHashCodeUseCase;

    @Autowired
    ApplicationContext applicationContext;

    /**
     * Converts a Java object into JSON representation
     */
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
