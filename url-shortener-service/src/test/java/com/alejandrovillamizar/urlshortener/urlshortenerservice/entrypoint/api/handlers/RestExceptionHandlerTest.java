package com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.handlers;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.BusinessErrorMessage;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.domain.exceptions.BusinessException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class RestExceptionHandlerTest {

    private static RestExceptionHandler restExceptionHandler;

    @BeforeAll
    public static void init(){
        restExceptionHandler = new RestExceptionHandler();
    }

    @Test
    public void handleHashCodeCreationMaximumRetriesReachedExceptionShouldReturnHttpConflictStatus() {

        ResponseEntity<Object> responseEntity = restExceptionHandler.handleHashCodeCreationMaximumRetriesReachedException(new BusinessException(BusinessErrorMessage.HASH_CODE_CREATION_MAXIMUM_RETRIES_REACHED, ""));

        Assertions.assertEquals(HttpStatus.CONFLICT.value(), responseEntity.getStatusCode().value());
    }

    @Test
    public void handleNotFoundExceptionShouldReturnHttpNotFoundStatus() {

        ResponseEntity<Object> responseEntity = restExceptionHandler.handleNotFoundException(new BusinessException(BusinessErrorMessage.REGISTRY_NOT_FOUND, ""));

        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseEntity.getStatusCode().value());
    }

    @Test
    public void handleExceptionShouldReturnHttpInternalServerErrorStatus() {

        ResponseEntity<Object> responseEntity = restExceptionHandler.handleException(new Exception());

        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), responseEntity.getStatusCode().value());
    }


}
