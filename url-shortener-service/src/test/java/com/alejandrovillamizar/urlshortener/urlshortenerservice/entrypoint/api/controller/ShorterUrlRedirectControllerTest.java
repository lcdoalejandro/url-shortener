package com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.controller;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.models.HashedUrlModel;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.AbstractBasicTest;
import org.junit.jupiter.api.Test;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ShorterUrlRedirectControllerTest extends AbstractBasicTest {

    @Test
    public void redirectShorterUrlShouldMovePermanently() throws Exception {

        String shortLinkHashCode = "fx4frjyn";
        String expectedUrl = "http://test.test";

        Optional<HashedUrlModel> maybeHashedUrlModel = Optional.of(
                HashedUrlModel.builder()
                        .hashCode(shortLinkHashCode)
                        .url(expectedUrl)
                        .build()
        );

        when(cacheManager.getCache(any())).thenReturn(new ConcurrentMapCache("hashCode"));
        when(mongoHashedUrlRepository.findById(shortLinkHashCode)).thenReturn(maybeHashedUrlModel);

        this.mockMvc.perform(
                get(new StringBuilder().append("/").append(shortLinkHashCode).toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isMovedPermanently())
                .andExpect(header().exists(HttpHeaders.LOCATION))
                .andExpect(header().string(HttpHeaders.LOCATION, is(expectedUrl)));
    }

    @Test
    public void redirectShorterUrlShouldReturnRegistryNotFound() throws Exception {

        String shortLinkHashCode = "fx4frjyn";

        Optional<HashedUrlModel> maybeHashedUrlModel = Optional.empty();

        when(cacheManager.getCache(any())).thenReturn(new ConcurrentMapCache("hashCode"));
        when(mongoHashedUrlRepository.findById(shortLinkHashCode)).thenReturn(maybeHashedUrlModel);

        this.mockMvc.perform(
                get(new StringBuilder().append("/").append(shortLinkHashCode).toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
