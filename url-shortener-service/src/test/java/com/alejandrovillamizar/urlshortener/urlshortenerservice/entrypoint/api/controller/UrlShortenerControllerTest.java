package com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.controller;

import com.alejandrovillamizar.urlshortener.urlshortenerservice.data.storage.models.HashedUrlModel;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.AbstractBasicTest;
import com.alejandrovillamizar.urlshortener.urlshortenerservice.entrypoint.api.dto.UrlDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.http.MediaType;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UrlShortenerControllerTest extends AbstractBasicTest {

    @Test
    public void shortenUrlShouldReturnOk() throws Exception {

        String host = "http://127.0.0.1:8086/";
        String expectedHashCode = "abcde693";
        String expectedUrl = "http://test.test";
        String expectedShortUrl = new StringBuilder().append(host).append(expectedHashCode).toString();

        UrlDto urlDto = UrlDto.builder().url(expectedUrl).build();

        HashedUrlModel hashedUrlModel = HashedUrlModel.builder()
                .hashCode(expectedHashCode)
                .url(expectedUrl)
                .createAt(new Date())
                .build();

        when(mongoHashedUrlRepository.save(any())).thenReturn(hashedUrlModel);

        this.mockMvc.perform(
                post("/api/v1/shorturls/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(urlDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.hashCode", is(expectedHashCode)))
                .andExpect(jsonPath("$.url", is(expectedUrl)))
                .andExpect(jsonPath("$.shortUrl", is(expectedShortUrl)));
    }

    @Test
    public void shortenEmptyUrlShouldReturnBadRequest() throws Exception {

        UrlDto emptyUrlDto = UrlDto.builder().url("").build();

        this.mockMvc.perform(
                post("/api/v1/shorturls/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(emptyUrlDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shortenUrlWithOnlySpacesShouldReturnBadRequest() throws Exception {

        UrlDto emptyUrlDto = UrlDto.builder().url("  ").build();

        this.mockMvc.perform(
                post("/api/v1/shorturls/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(emptyUrlDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getUrlByShortLinkHashCodeShouldReturnCorrespondentUrl() throws Exception {

        String shortLinkHashCode = "fx4frjyn";
        String expectedUrl = "http://test.test";

        Optional<HashedUrlModel> maybeHashedUrlModel = Optional.of(
                HashedUrlModel.builder()
                        .hashCode(shortLinkHashCode)
                        .url(expectedUrl)
                        .build()
        );

        when(cacheManager.getCache(any())).thenReturn(new ConcurrentMapCache("hashCode"));
        when(mongoHashedUrlRepository.findById(shortLinkHashCode)).thenReturn(maybeHashedUrlModel);

        this.mockMvc.perform(
                get(new StringBuilder().append("/api/v1/shorturls/").append(shortLinkHashCode).toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.url", is(expectedUrl)));
    }

    @Test
    public void deleteUrlByShortLinkHashCodeShouldShouldReturnNotContentStatus() throws Exception {

        String expectedShortLinkHashCode = "fx4frjyn";
        String expectedUrl = "http://test.test";

        Optional<HashedUrlModel> maybeHashedUrlModel = Optional.of(
                HashedUrlModel.builder()
                        .hashCode(expectedShortLinkHashCode)
                        .url(expectedUrl)
                        .build()
        );

        when(cacheManager.getCache(any())).thenReturn(new ConcurrentMapCache("hashCode"));
        when(mongoHashedUrlRepository.findById(expectedShortLinkHashCode)).thenReturn(maybeHashedUrlModel);

        this.mockMvc.perform(
                delete(new StringBuilder().append("/api/v1/shorturls/").append(expectedShortLinkHashCode).toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        ArgumentCaptor<HashedUrlModel> hashedUrlModelArgument = ArgumentCaptor.forClass(HashedUrlModel.class);
        verify(mongoHashedUrlRepository).delete(hashedUrlModelArgument.capture());

        Assertions.assertEquals(expectedShortLinkHashCode, hashedUrlModelArgument.getValue().getHashCode());
    }

    @Test
    public void deleteUrlByShortLinkHashCodeShouldShouldReturnNotFoundStatus() throws Exception {

        String shortLinkHashCode = "fx4frjyn";

        Optional<HashedUrlModel> maybeHashedUrlModel = Optional.empty();

        when(cacheManager.getCache(any())).thenReturn(new ConcurrentMapCache("hashCode"));
        when(mongoHashedUrlRepository.findById(shortLinkHashCode)).thenReturn(maybeHashedUrlModel);


        this.mockMvc.perform(
                delete(new StringBuilder().append("/api/v1/shorturls/").append(shortLinkHashCode).toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}
