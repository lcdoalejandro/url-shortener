package com.alejandrovillamizar.urlshortener.urlshortenerapigateway.configuration;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestAPIGatewayRoutes {

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/api/v1/shorturls/**")
                        .filters(f ->
                                f.rewritePath("/service(?<segment>/?.*)", "$\\{segment}")
                                .addRequestHeader("X-shorturls-Header", "url-shortener-service-header"))
                        .uri("http://url-shortener-service:8088/")
                        .id("url-shortener-service"))
                .route(r -> r.path("/**")
                        .filters(f ->
                                f.rewritePath("/service(?<segment>/?.*)", "$\\{segment}")
                                        .addRequestHeader("X-shorturls-Header", "url-shortener-redirect-header"))
                        .uri("http://url-shortener-service:8088/")
                        .id("url-shortener-redirect")
                )
                .build();
    }
}
