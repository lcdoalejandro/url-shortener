package com.alejandrovillamizar.urlshortener.urlshortenerapigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class UrlShortenerApiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(UrlShortenerApiGatewayApplication.class, args);
    }

}
