echo "\n Loading openjdk11 docker image"
docker load -i url-shortener-devops/docker-images/openjdk11.docker

echo "\n Loading mongo3.6 docker image"
docker load -i url-shortener-devops/docker-images/mongo3.6.docker

echo "\n Loading redis docker image"
docker load -i url-shortener-devops/docker-images/redis.docker

echo "\n Loading url-shortener-discovery-server docker image"
docker load -i url-shortener-devops/docker-images/url-shortener-discovery-server.docker

echo "\n Loading url-shortener-api-gateway docker image"
docker load -i url-shortener-devops/docker-images/url-shortener-api-gateway.docker

echo "\n Loading url-shortener-service docker image"
docker load -i url-shortener-devops/docker-images/url-shortener-service.docker

echo "\n Starting services orchestration with pre-loaded java projects images"
docker-compose -f url-shortener-devops/docker-compose-run-with-project-images.yml up
